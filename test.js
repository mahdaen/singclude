/* Loading IncludeJS */
require('./index.js');

include.global({
    File   : 'fs',
    Exec   : 'vm',
});

console.log(File, Exec);

var pnow, plas;

/* Try profile information using include */
console.log('Test: Include profile from test');

/* Test include */
pnow = process.hrtime();

var initial = include('test/profile');

plas = process.hrtime();
console.log('\r\nInclude INITIAL finished in: ' + (plas[ 1 ] - pnow[ 1 ]) / 100000 + 'ms\r\n');

/* Check the result */
console.log(initial);

/* Test Include Cached */
pnow = process.hrtime();

var infos = include('test/profile');

plas = process.hrtime();
console.log('\r\nInclude CACHED finished in: ' + (plas[ 1 ] - pnow[ 1 ]) / 100000 + 'ms\r\n');

/* Check cached result */
console.log(infos);

/* Test Include Cached */
pnow = process.hrtime();

var nochaced = include.incache('test/profile');

plas = process.hrtime();
console.log('\r\nInclude RECOMPILE finished in: ' + (plas[ 1 ] - pnow[ 1 ]) / 100000 + 'ms\r\n');

/* Check cached result */
console.log(nochaced);

/* Test Include Cached */
pnow = process.hrtime();

var recompile = include.nocache('test/profile');

plas = process.hrtime();
console.log('\r\nInclude FORCE NO-CACHE finished in: ' + (plas[ 1 ] - pnow[ 1 ]) / 100000 + 'ms\r\n');

/* Check cached result */
console.log(recompile);

/* Log Name from Extend */
logName();

/* Test Get Raw Scripts */
var contents = include.content('test/people');
console.log(contents);

/* Check Caches */
console.log(Object.keys(include.cache));
