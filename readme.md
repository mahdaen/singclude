# Singclude - A Small Scripts Importer

[![build status](https://ci.gitlab.com/projects/6731/status.png?ref=master)](https://ci.gitlab.com/projects/6731?ref=master)

Sinclude is a small NodeJS scripts importer. Singclude works just like **`require()`**, but offering inline imports, where imported scripts using inline import will eavaluated in single context. So each files can communicate with each others without **`module.exports`** needed.

### **Get Started**
***

Let's create a simple sample:

**`main.js`**
```js
// Load singclude
require('singclude');

// Import script
var file = include('subdir/file');

console.log(file.reader, MODULE_VERSION);
```

**`subdir/file.js`**
```js
// Inline imports
'@require util';
'@include tool, cli/handler.js, ./laoder';

var isstr = util.isString('Foo Bar');

// Also support json
'@include ./package.json';

// Extend global object.
extend({
  MODULE_VERSION: package.version
});

// Export module
module.exports = {
  reader = 'I am reader',
  writer = 'I am writer',
  pkgver = package.version,
  parser = function() {
    '@include cli/parser';
  }
}
```

**`subdir/util.js`**
```js
// Native require is fine.
var util = require('util');

// No need to re-require singclude.
var test = include('./test', { util: util, name: 'John' });
```

**`subdir/tool.js`**
```js
var isdate = util.isDate('Dec 28, 2001');
```

**`package.json`**
```json
{
  "name": "Package",
  "version": "1.0.0"
}
```

**`test.js`**
```js
console.log(util, name);
```

***
**Sample Notes**
- Scripts that imported using **`include()`** module will be evaluated in new context, just like what **`require()`** do.
- Importing folder using **`include()`** will returns an object contains result from each files, where filename (without extension) will be used as property on main result. E.g: **`RESULT { 'file-a': RESULT, 'file-b': RESULT, 'file-c': JSONRESULT }`**
- Importing using **`include()`** can accepts shared variable, and imported script can access those variables.
- Scripts that imported using inline import (**`@require`** and **`@include`**) is evaluated in the same context of importer.
- Scripts imported using inline import can access everything inside the importer context.
- Default lookup directory is relative to the current files path. Using **`./`** in filename will lookup the script from the project root, using **`/`** will using absolute path.
- File extension if optional. If importer find a folder, then all **`js/json`** files inside will be imported.
- Importing JSON using inline import will wrap the result with new variable named with filename, invalid characters for variable name in filename will be converted to **`_`**.
- Inline importer can be enywhere, even insdie a function block (except in json file).
- Inline importer should be inside **`'`** or **`"`**, like **`'use strict';`** usage.
- Inline importer accepts multiple import in single line. E.g: **`'@require libs/folder, libs/cli, libs/app.js';`**

### Methods

***
#### **`include()`**

Scripts importer module, import scripts using new context and makes all inline importt inside that is running in the same context. **`include()`** will export the result like when using **`require()`**.

**Usage**
```js
include(file/dir, sharedvar);
```

* **`file/dir`** - [**required**] - File/Directory to import. Accepted file type is **`.js`** and **`.json`**
* **`sharedvar`** - [**optional**] - Shared variables, an object contains property as variable name, and value as variable value.

See include in **`subdir/util.js`** above.

***
#### **`@require`**

Inline script importer, import scripts and evaluate it in the same context of the importer. If the required file is not exist, then will throwing error.

**Usage**
```js
'@require file/dir';
```

* **`file/dir`** - [**required**] - A file/folder to import.

See **`subdir/file.js`** above.

***
#### **`@include`**

Inline script importer, import and evaluate it in the same context of the importer. Just like **`@require`**, but would not throwing error if requested cannot be found.

**Usage**
```js
'@include file/dir';
```

* **`file/dir`** - [**required**] - A file/folder to import.

See **`subdir/file.js`** above.

***
#### **`extend()`**

Global object extender, makes everything we extend to global is accessible from everywhere.

**Usage**
```js
global.extend(MODULES) or extend(MODULES);
```

* **MODULES** - An Object contains property as variable name, value as variable value.

See **`subdir/file.js`** above.

***
#### **`include.global()`***

Load modules to global object to makes them available on global.

**Usage**
```js
include.global(ALIAS, MODULE);
```

* **`ALIAS`** - [**`required`**] - Name of module to use in global. Use string for single include, use object for multiple include (property name as alias).
* **`MODULE`** - [**`required`**] - Module name to load. Required in single include.

**Example**
```js
// Single include.
include.global('Shell', 'child_process');

// Multiple include.
include.global({
  File: 'fs',
  Exec: 'vm'
});

console.log(Shell, File, Exec);
```

***
#### **`include.cache`**

Caches collection of imported scripts.

***
#### **`include.nocache()`**

Include scripts but ignore cache.

**Usage**
```js
include.nocache(file/dir, sharedvar);
```

***
#### **`include.incache()`**

Include scripts and re-compile cached contents if found.

**Usage**
```js
include.incache(file/dir, sharedvar);
```

***
### **`include.content()`**

Get raw content of imported scripts without running the scripts.

**Usage**
```js
include.nocache(file/dir);
```

