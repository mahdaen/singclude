/**
 * Singclude
 * A simple NodeJS scripts includer.
 * Singclude works just like require(), but offering inline import, like include and require in PHP.
 * Not only giving synchronus call, inline import provide an ability to communicate between imported scripts.

 * Copyright Nanang Mahdaen El Agung <nanang@mahdaen.name> (http://mahdaen.name).

 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:

 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

"use strict";

/* Loading Required Modules */
var path = require('path'),
    runr = require('vm'),
    file = require('fs'),
    home = path.resolve('./'),
    scwf = __filename,
    scwd = __dirname;

/* Creating Cache Collections */
var Cache = {};

/* Exporting globals */
global.require = require;
global.exports = exports;
global.extend  = extendGlobal;
global.module  = module;

/* Creating Script Includer */
var include = function ( request, args, option ) {
    var getname = getFilename(),
        dirname = path.dirname(getname),
        oldname = getname,
        olddirs = dirname;

    args = !'object' === typeof args ? {} : args;

    /* Change the lookup dir if required */
    if ( request.substr(0, 1) === '/' ) dirname = '';
    if ( request.substr(0, 2) === './' ) dirname = home;

    /* Parsing Shared Variables */
    var shared = [], arg, result;

    if ( request in Cache ) {
        if ( !option ) {
            /* Create result from cache */
            result = Cache[ request ];
        }
        else {
            if ( option === 'recompile' ) {
                var cached = Cache[ request ];

                /* Prepare Shares */
                bindShares();

                /* Re-Compile Parsed Contents */
                result = compileRequest(cached.constructor.contents, cached.constructor.filename);

                /* Reset Shares */
                resetShares();
            }
            else if ( option === 'reinit' ) {
                /* Prepare Shares */
                bindShares();

                /* Creating Result */
                result = parseInclude(request, dirname);

                /* Reset Shares */
                resetShares();
            }
            else if ( option === 'nocompile' ) {
                return Cache[ request ].constructor.contents;
            }
        }
    }
    else {
        if ( !option ) {
            /* Prepare Shares */
            bindShares();

            /* Creating Result */
            result = parseInclude(request, dirname);

            /* Adding to Cache Collections if not forced to no-cache */
            Cache[ request ] = result;

            /* Reset Shares */
            resetShares();
        }
        else if ( option === 'nocompile' ) {
            return parseInclude(request, dirname, true);
        }
    }

    /* Shares Binder */
    function bindShares () {
        /* Collecting non-global shared variables */
        for ( arg in args ) {
            if ( !global[ arg ] ) {
                shared.push(arg);
            }
        }

        /* Temporarily bind shared variables to global */
        shared.forEach(function ( arg ) {
            global[ arg ] = args[ arg ];
        });
    }

    /* Shares Resetter */
    function resetShares () {
        /* Revert global __filename and __dirname */
        global.__filename = oldname;
        global.__dirname  = olddirs;

        /* Remove temporarily shared variables from global */
        shared.forEach(function ( arg ) {
            delete global[ arg ];
        });
    }

    /* Return the result */
    return result;
}

/* Include Without Cache */
var nocache = function ( request, args ) {
    return include(request, args, 'reinit');
}

/* Include and recompile the cache if found */
var incache = function ( request, args ) {
    return include(request, args, 'recompile');
}

/* Include but don't compile */
var getraws = function ( request, args ) {
    return include(request, args, 'nocompile');
}

/* Include to Global */
var useGlobal = function ( alias, module ) {
    if ( 'string' === typeof alias && 'string' === typeof module ) {
        /* Continue load module if not loaded */
        if ( !global[ alias ] ) {
            var result = require(module);

            if ( result ) {
                global[ alias ] = result;
            }
        }
    }
    else if ( 'object' === typeof alias && !Array.isArray(alias) ) {
        for ( var name in alias ) {
            if ( alias.hasOwnProperty(name) ) {
                useGlobal(name, alias[ name ]);
            }
        }
    }
}

/* Include Extension */
include.cache   = Cache;
include.global  = useGlobal;
include.nocache = nocache;
include.incache = incache;
include.content = getraws;

/* Exporting Includer */
global.include = include;

/* Export Include Module */
module.exports = include;

/* Request Parser */
function parseInclude ( request, dirname, getraw ) {
    request = request.replace(/^\.\//, '');

    var filestat, filename, content;

    try {
        filename = dirname + '/' + request;
        filestat = file.statSync(filename);
    }
    catch ( err ) {
        try {
            filename = dirname + '/' + request + '.js';
            filestat = file.statSync(filename);
        }
        catch ( err ) {
            var error = new Error('Cannot find file/directory \'' + request);

            error.stack = getStack(error);

            console.log(error);

            throw error;
        }
    }

    if ( filestat.isDirectory() ) {
        var result = {}, files, pkg;

        /* Read directory contents */
        files = file.readdirSync(filename);

        /* Convert result to string if no compile is requested */
        if ( getraw ) result = '';

        /* Iterate files and process, js and json files */
        for ( var i = 0; i < files.length; ++i ) {
            var usefile = files[ i ],
                extfile = path.extname(usefile);

            if ( extfile === '.js' || extfile === '.json' ) {
                var bsmname, orgname;

                dirname = filename;
                orgname = dirname + '/' + usefile;

                content = file.readFileSync(dirname + '/' + usefile, 'utf8');

                if ( extfile === '.json' ) {
                    bsmname = path.basename(usefile, '.json');
                }
                else if ( extfile === '.js' ) {
                    bsmname = path.basename(usefile, '.js');

                    content = stripBOM(content).replace(/^\#\!.*/, '');
                    content = parseImport(content, dirname, usefile);
                }

                /* Append contents to result if no compile is requested */
                if ( !getraw ) {
                    result[ bsmname ] = compileRequest(content, orgname);
                }
                /* Append compiled to result if compile is requested */
                else {
                    if ( extfile === '.json' ) {
                        result += 'var ' + bsmname + ' = ' + content + '\r\n';
                    }
                    else {
                        result += content + '\r\n';
                    }
                }
            }
        }

        /* Return result */
        return result;
    }
    else if ( filestat.isFile() ) {
        content = file.readFileSync(filename, 'utf8');

        if ( path.extname(filename) === '.js' ) {
            dirname = path.dirname(filename);

            content = stripBOM(content).replace(/^\#\!.*/, '');
            content = parseImport(content, dirname, filename);
        }

        /* Return raw scripts or compiled result */
        if ( !getraw ) {
            return compileRequest(content, filename);
        }
        else {
            return content;
        }
    }
}

/* Request Compiler */
function compileRequest ( content, filename ) {
    var result, error;

    if ( path.extname(filename) === '.json' ) {
        try {
            result = JSON.parse(content);
        }
        catch ( err ) {
            error = new Error(err.message + '\r\n    at ' + filename);

            error.stack = getStack(error);

            console.log(error);

            throw error;
        }

        return result;
    }
    else {
        /* Temporarily replace the global __filename and __dirname with current file and directory */
        global.__filename = filename;
        global.__dirname  = path.dirname(filename);

        try {
            result = runr.runInThisContext(content, { filename : filename });
        }
        catch ( err ) {
            if ( err.code === "MODULE_NOT_FOUND" ) {
                try {
                    result = require(filename);
                }
                catch ( err ) {
                    error = new Error(err.message + '\r\n    at ' + filename);

                    error.stack = getStack(error);

                    console.log(error);

                    throw error;
                }
            }
            else {
                error = new Error(err.message + '\r\n    at ' + filename);

                error.stack = getStack(error);

                console.log(error);

                throw error;
            }
        }

        if ( result ) {
            result.constructor.contents = content;
            result.constructor.filename = filename;

            return result;
        }
        else {
            return {};
        }
    }
}

/* Byte order marker remover. */
function stripBOM ( content ) {
    if ( content.charCodeAt(0) === 0xFEFF ) {
        content = content.slice(1);
    }

    return content;
}

/* Import Parser */
function parseImport ( content, dirname, filename ) {
    var i, pattern, result;

    if ( 'string' === typeof content ) {
        /* Parse required files/folders. */
        var requires = content.match(/[\/\\\*\s\'\"]+\@require\s+[a-zA-Z\-\_\.\,\/\\\s]+[\'\"]{1}[\;]?[\r\n]?/g);

        /* If found, parse them and ensure they're exist on the system. */
        if ( requires ) {
            /* Parse each pattern. */
            for ( i = 0; i < requires.length; ++i ) {
                pattern = requires[ i ].replace(/^[\s\/\*]+/, '').replace(/[\r\n]+/g, '');

                if ( pattern.substr(0, 1) !== '/' && pattern.substr(0, 1) !== '*' ) {
                    result = parseContent(pattern, '@require', false, dirname, filename);

                    content = content.replace(pattern, result).replace(/[\r\n]{2,}/g, '\r\n');
                }
            }
        }

        /* Parse included files/folders. */
        var includes = content.match(/[\/\\\*\s\'\"]+\@include\s+[a-zA-Z\-\_\.\,\/\\\s]+[\'\"]{1}[\;]?[\r\n]?/g);

        /* If found, parse them. */
        if ( includes ) {
            /* Parse each pattern. */
            for ( i = 0; i < includes.length; ++i ) {
                pattern = includes[ i ].replace(/^[\s\/\*]+/, '').replace(/[\r\n]+/g, '');

                if ( pattern.substr(0, 1) !== '/' && pattern.substr(0, 1) !== '*' ) {
                    result = parseContent(pattern, '@include', false, dirname, filename);

                    content = content.replace(pattern, result).replace(/[\r\n]{2,}/g, '\r\n\r\n');
                }
            }
        }
    }

    return content;
}

/* Imports Content Parser */
function parseContent ( pattern, replace, required, dirname, filename ) {
    if ( pattern.substr(0, 1) === '/' ) return '';

    var content = '',
        lastdir = '',
        flpath  = '';

    var files = pattern
        .replace(replace, '')
        .replace(/[\'\"\;\s]+/g, '')
        .split(',');

    var errStacks,
        error;

    for ( var i = 0; i < files.length; ++i ) {
        var name = files[ i ], exist;

        /* Holding last dir */
        lastdir = dirname;

        /* Change the lookup dir if required */
        if ( name.substr(0, 1) === '/' ) dirname = '';
        if ( name.substr(0, 2) === './' ) dirname = home;

        /* Remove self-dir mark. */
        name = name.replace(/^\.\//, '');

        try {
            flpath = dirname + '/' + name;
            exist  = file.statSync(flpath);
        }
        catch ( err ) {
            try {
                flpath = dirname + '/' + name + '.js';
                exist  = file.statSync(flpath);
            }
            catch ( err ) {
                if ( required ) {
                    /* Recreate new error with message. */
                    error = new Error('Cannot find required file/directory \'' + name + '\'\r\n    at ' + filename);

                    error.stack = getStack(error);

                    /* Throw the error. */
                    console.log(error);

                    throw error;
                }
                else {
                    exist = undefined;

                    console.log('Warning: Include skipped. Cannot find file/directory \'' + name + '\'\r\n         at ' + filename);
                }
            }
        }

        if ( exist ) {
            if ( exist.isDirectory() ) {
                var dirs = file.readdirSync(flpath);

                for ( var x = 0; x < dirs.length; ++x ) {
                    var src = dirs[ x ];

                    var flname = flpath + '/' + src;

                    dirname = path.dirname(flname);

                    if ( path.extname(src) === '.js' ) {
                        content += parseImport('\r\n' + file.readFileSync(flname, 'utf8'), dirname, flname);
                    }
                    else if ( path.extname(src) === '.json' ) {
                        content += '\r\nvar ' + path.basename(flname, '.json').replace(/[\-\.]+/g, '_') + ' = ' + file.readFileSync(flname, 'utf8') + '\r\n';
                    }
                }
            }
            else if ( exist.isFile() ) {
                dirname = path.dirname(flpath);

                if ( path.extname(flpath) === '.js' ) {
                    content += parseImport('\r\n' + file.readFileSync(flpath, 'utf8'), dirname, flpath);
                }
                else if ( path.extname(flpath) === '.json' ) {
                    content += '\r\nvar ' + path.basename(flpath, '.json').replace(/[\-\.]+/g, '_') + ' = ' + file.readFileSync(flpath, 'utf8') + '\r\n';
                }
            }
        }

        /* Restore last dir */
        dirname = lastdir;
    }

    return stripBOM(content);
}

/* Extend Globals */
function extendGlobal ( modules, value ) {
    if ( 'string' === typeof modules ) {
        global[ modules ] = value;
    }
    else {
        if ( 'object' === typeof modules && !Array.isArray(modules) ) {
            for ( var key in modules ) {
                if ( modules.hasOwnProperty(key) ) {
                    global[ key ] = modules[ key ];
                }
            }
        }
    }

    return modules;
}

/* Caller Filename */
function getFilename ( raws ) {
    var stack = new Error().stack.split(/\s+at\s+/).slice(1), done, caller;

    while ( stack.length && !done ) {
        if ( stack[ 0 ].search(scwd) < 0 ) {
            if ( raws ) {
                return stack;
            }
            else {
                caller = stack[ 0 ].match(/[a-zA-Z\d\.\-\_\:\/\\\:\)]+$/);

                if ( caller ) {
                    caller = caller[ 0 ].replace(/[\(\)]+/g, '').split(':')[ 0 ];

                    return caller;
                }
            }
        }

        stack = stack.slice(1);
    }

    return undefined;
}

/* Error Stack Getter */
function getStack ( err ) {
    var stack = err.stack.split(/\s+at\s+/), newstack, rests = [];

    while ( stack.length ) {
        if ( stack[ 0 ].search(scwd) < 0 ) {
            rests.push(stack[ 0 ]);
        }

        stack = stack.slice(1);
    }

    return newstack = rests.join('\r\n       at ');
}