var profile = [];

console.log('Test: include all files in people folder');

'@include people';

console.log('Test: include form');

'@include form';

console.log('Test: include user from current dir and package.json from project root (cwd)');

var user = include('user'), pkg = include('./package.json');

console.log('Test: include package.json from project root using inline import');

'@include ./package.json';

console.log('Test: include multiple files');

'@include users/a, users/b';

function logName () {
    console.log('John');
}

extend({
    logName : logName
});

module.exports = profile;
